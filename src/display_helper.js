// HELPER D'AFFICHAGE


const VALUE_EMPTY = 0
const VALUE_DROPLET = 1
const VALUE_PLAYER = 2
const VALUE_DROPLET_ON_PLAYER = 3

const CHAR_EMPTY = ' '
const CHAR_DROPLET = `<img src='./src/img/space2.jpg'>`
const CHAR_PLAYER = `<img src='./src/img/player.jpg'>`
const CHAR_DROPLET_ON_PLAYER = `<img src='./src/img/crash.jpeg'>`
let player= document.getElementById('70');


// helper d'affichage
const DisplayHelper = {

    // création de la chaine d'affichage à partir de la matrice du jeu
    makeMatrixString(matrix, playerPosition) {

        for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < matrix[i].length; j++) 
          
            
            {

                if (matrix[i][j] == 1) {
                    document.getElementById(`${i}${j}`).innerHTML = CHAR_DROPLET
                }
                else {
                    document.getElementById(`${i}${j}`).innerHTML = CHAR_EMPTY
                }

                if (i === matrix.length - 1) {
                    if (j === playerPosition) {
                        document.getElementById(`${i}${j}`).innerHTML = CHAR_PLAYER;
                    }
                    if (matrix[i][playerPosition] === 1) {
                        document.getElementById(`${i}${playerPosition}`).innerHTML = CHAR_DROPLET_ON_PLAYER;
                    }
                }
            }

        }

    }
}