//import Game from "sweaty-promo-client/src/offline";

// instance de la class du jeu
var game = new SweatyPromoClient.offline()

// zone d'affichage
var displayZone = document.getElementById('displayZone')

// nouvelle matrice des gouttes
game.on('matrix', (matrix) => {
    updateDisplay(matrix)
    var position = game.getPosition()
    var line6 = game.getMatrix()[6]
    var line5 = game.getMatrix()[5]
    var line4 = game.getMatrix()[4]

    for (let index = 0; index < line6.length; index++) {
        if (line6[index] === 1 && position === index) {


            if (position === 0) {
                game.right();
                break;
            }
            else if (position === 4) {
                game.left();
                break;
            }

        }
        else {
            if (line5[position] === 1) {
                game.right();
                break;
            }

            
            else if  (line5[position - 1] === 1) {
                game.left();
                break;
            }
        }
    }

});



// perdu, en sueur
game.on('sweaty', () => {
    document.getElementById('gameOverMessage').style.visibility = 'visible'
    document.getElementById('startButton').style.visibility = 'visible'
    console.log('En sueur !')
})

// mise à jour de l'affichage
function updateDisplay(matrix) {
    if (!matrix) {
        matrix = game.getMatrix()
    }
    // création de la chaine d'affichage de la matrice
    var matrixString = null

    matrixString = DisplayHelper.makeMatrixString(matrix, game.getPosition())

    // actualisation de l'affichage

}

// bouton de lancement
function startButton() {
    document.getElementById('gameOverMessage').style.visibility = 'hidden'
    document.getElementById('startButton').style.visibility = 'hidden'

    game.start()
}

// bouton vers la gauche
function leftButton() {
    game.left()
    //
    updateDisplay()
}

// bouton vers la droite
function rightButton() {
    game.right()
    //
    updateDisplay()
}

// première affichage


// auto start
//startButton()
